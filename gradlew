package com.example.pokebatoblt.battle

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.example.pokebatoblt.R
import com.example.pokebatoblt.enumclass.Phases
import com.example.pokebatoblt.enumclass.Stats
import com.example.pokebatoblt.enumclass.User
import com.example.pokebatoblt.logic.*
import kotlin.random.Random

class BattleActivity : AppCompatActivity(), Runnable, BattleCmdFragment.BattleCmdFragmentCallback,
    BattleViewFragment.BattleViewFragmentCallback, BattleTextFragment.BattleTextFragmentCallback {

    private lateinit var battleViewFragment: BattleViewFragment
    private lateinit var battleTextFragment: BattleTextFragment
    private lateinit var battleCmdFragment: BattleCmdFragment

    private val partyP: MutableList<Pokemon>
    private val partyE: MutableList<Pokemon>
    private var mainPokemonP: Pokemon
    private var mainPokemonE: Pokemon

    private val handler = Handler()
    private var isLoadingBattleTextFragment = false
    private var phases: Phases = Phases.START_TURN
        set(value) {
            var r: Runnable? = null
            r = Runnable {
                if (isLoadingBattleTextFragment)
                    handler.postDelayed(r, 1)
                else
                    field = value
            }
            handler.post(r)
        }

    /** Triple<先攻ユーザー, 先攻ポケモンの技, 後攻ポケモンの技> */
    private var atkStack: Triple<User, Move?, Move?> = Triple(User.PLAYER, null, null)

    init {
        val pokeMapP = PokemonMap()
        val pokeMapE = PokemonMap()
        partyP = mutableListOf(pokeMapP.get("ピカチュウ"), pokeMapP.get("フシギバナ"), pokeMapP.get("リザードン"))
        partyE = mutableListOf(pokeMapE.get("フシギバナ"), pokeMapE.get("リザードン"), pokeMapE.get("カメックス"))
        mainPokemonP = partyP.first()
        mainPokemonE = partyE.first()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_battle)

        initFragment()
    }

    override fun onResume() {
        super.onResume()
        handler.post(this)
    }

    private fun initFragment() {
        val transaction = supportFragmentManager.beginTransaction()
        battleCmdFragment = BattleCmdFragment.newInstance(partyP.first().moveList)
        battleTextFragment = BattleTextFragment()
        battleViewFragment = BattleViewFragment()
        transaction
            .replace(R.id.battle_cmd_layout, battleCmdFragment, null)
            .replace(R.id.battle_text_layout, battleTextFragment, null)
            .replace(R.id.battle_view_layout, battleViewFragment, null)
            .commit()
    }

    override fun run() {
        val firstStrikeUser = atkStack.first
        val secondStrikeUser = firstStrikeUser.reverse()

        when (phases) {
            Phases.START_TURN -> {
                atkStack = Triple(checkFirstStrikeUser(), null, null)
                battleViewFragment.setStatsBarLayoutCallback(atkStack.first)
                phases = Phases.WAIT_ATTACK
            }

            Phases.WEATHER_CHECK -> {
            }

            Phases.STATUS_AILMENT_CHECK -> {
            }

            Phases.WAIT_ATTACK -> {
            }

            Phases.ATTACK1 -> {
                battleCmdFragment.disableCmdButton()

                if (atkStack.second == null) {
                    handler.postDelayed(this, 1)
                    return
                }

                val dmg = calDmg(firstStrikeUser, atkStack.second)
                attack(firstStrikeUser, dmg)

                battleTextFragment.addAtkLog(getMainPokemon(firstStrikeUser), atkStack.second)

                atkStack = Triple(firstStrikeUser, null, atkStack.third)
            }

            Phases.COMPLETE_ATTACK1 -> {
                setPhasesWaitLoading(Phases.ATTACK2)
            }

            Phases.ATTACK2 -> {
                if (atkStack.third == null) {
                    handler.postDelayed(this, 1)
                    return
                }
                val dmg = calDmg(secondStrikeUser, atkStack.third)
                attack(secondStrikeUser, dmg)

                battleTextFragment.addAtkLog(getMainPokemon(secondStrikeUser), atkStack.third)

                atkStack = Triple(firstStrikeUser, atkStack.second, null)
            }

            Phases.COMPLETE_ATTACK2 -> {
                setPhasesWaitLoading(Phases.END_TURN)
            }

            Phases.END_TURN -> {
                battleCmdFragment.enableCmdButton()
                setPhasesWaitLoading(Phases.START_TURN)
            }
        }
        handler.postDelayed(this, 1)
    }

    /**
     * Sを見て先攻がどっちか判定
     * 同側の場合ランダムのため、複数回実効ぜずターンの開始自に一回だけ実行する
     * @return 先攻User
     */
    private fun checkFirstStrikeUser(): User {
        val speedP = mainPokemonP.getRankAppliedStats()[Stats.S.ordinal]
        val speedE = mainPokemonE.getRankAppliedStats()[Stats.S.ordinal]
        return when {
    