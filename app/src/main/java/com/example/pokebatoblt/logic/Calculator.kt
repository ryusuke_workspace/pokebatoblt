package com.example.pokebatoblt.logic

import com.example.pokebatoblt.enumclass.*
import kotlin.random.Random

/**
 * 計算を行うクラス
 */
class Calculator {
    companion object {
        /**
         * ダメージ計算のプロトコル
         *
         * @param dmg ダメージ
         * @param effective Effective
         * @param isCritical 急所に当たった場合true
         */
        class CalDmgProtocol(
            val dmg: Int,
            val effective: Effective = Effective.NORMAL,
            val isCritical: Boolean = false
        )

        /**
         * ダメージ計算
         *
         * @param atkPokemon 攻撃ポケモン
         * @param defPokemon 防御ポケモン
         * @param move 攻撃技
         * @return CalDmgProtocol
         */
        fun calDmg(
            atkPokemon: Pokemon,
            defPokemon: Pokemon,
            move: Move
        ): CalDmgProtocol {

            // 急所判定
            var isCritical = false
            if (Random(System.currentTimeMillis()).nextInt(24) == 0)
                isCritical = true


            val atkPower = when (move.category) {
                MoveCategory.PHYSICAL -> {
                    if (atkPokemon.statusAilment1 == StatusAilment1.BURN)
                        atkPokemon.getRankAppliedStats(Stats.A) / 2
                    else
                        atkPokemon.getRankAppliedStats(Stats.A)
                }
                MoveCategory.SPECIAL -> atkPokemon.getRankAppliedStats(Stats.C)
                MoveCategory.STATUS -> return CalDmgProtocol(0)
            }
            val defPower = when (move.category) {
                MoveCategory.PHYSICAL -> {
                    if (isCritical)
                        defPokemon.getRealStats(Stats.B)
                    else
                        defPokemon.getRankAppliedStats(Stats.B)
                }
                MoveCategory.SPECIAL -> {
                    if (isCritical)
                        defPokemon.getRealStats(Stats.B)
                    else
                        defPokemon.getRankAppliedStats(Stats.D)
                }
                MoveCategory.STATUS -> return CalDmgProtocol(0)
            }


            // 基本計算
            var dmg = ((((atkPokemon.level * 2 / 5 + 2) * move.power).toFloat()
                    * atkPower.toFloat() / defPower.toFloat()).toInt().toFloat()
                    / 50f + 2f).toInt()

            // 乱数計算
            dmg = (dmg.toFloat()
                    * Random(System.currentTimeMillis()).nextInt(85, 100).toFloat() / 100f).toInt()

            // タイプ一致計算
            if (move.type == atkPokemon.type1 || move.type == atkPokemon.type2)
                dmg = (dmg.toFloat() * 1.5f).toInt()

            // タイプ相性
            val magnification = magnification(move.type, defPokemon.type1, defPokemon.type2)
            val effective: Effective = when {
                magnification == 0f -> Effective.NOT_EFFECTIVE
                magnification >= 2f -> Effective.SUPER_EFFECTIVE
                magnification < 1f -> Effective.NOT_VERY_EFFECTIVE
                else -> Effective.NORMAL
            }
            dmg = (dmg.toFloat() * magnification).toInt()

            // 急所
            if (isCritical)
                dmg = (dmg.toFloat() * 1.5f).toInt()

            return CalDmgProtocol(dmg, effective, isCritical)
        }

        /**
         * タイプ相性の計算
         *
         * @param atkType 攻撃技のタイプ
         * @param defenceType1　防御するポケモンのタイプ1
         * @param defenceType2 防御するポケモンのタイプ2
         */
        private fun magnification(atkType: Type, defenceType1: Type, defenceType2: Type): Float {
            val com1 = TypeCompatibility.list[atkType.ordinal][defenceType1.ordinal].let {
                if (it == 3f) 0.5f
                else it
            }
            val com2 = TypeCompatibility.list[atkType.ordinal][defenceType2.ordinal].let {
                if (it == 3f) 0.5f
                else it
            }
            return com1 * com2
        }

        /**
         * ステータスランクの計算（A,B,C,D,S）
         *
         * @param rank ステータスランク
         * @return ランクによる倍率
         */
        fun calStatsRankMultiplier(rank: Int): Float {
            return when {
                rank > 0 -> (2 + rank).toFloat() / 2f
                rank < 0 -> 2f / (2 + rank).toFloat()
                else -> 1f
            }
        }

        /**
         * S判定
         * 同側の場合ランダムのため、複数回実効ぜずターンの開始自に一回だけ実行する
         *
         * @param pokemonP プレイヤーのポケモン
         * @param pokemonE 相手のポケモン
         * @return 先攻User
         */
        fun checkFirstStrikeUser(pokemonP: Pokemon, pokemonE: Pokemon): User {
            var speedP = pokemonP.getRankAppliedStats(Stats.S)
            if (pokemonP.statusAilment1 == StatusAilment1.PARALYSIS)
                speedP /= 2

            var speedE = pokemonE.getRankAppliedStats(Stats.S)
            if (pokemonE.statusAilment1 == StatusAilment1.PARALYSIS)
                speedE /= 2

            return when {
                speedP > speedE -> User.PLAYER
                speedE > speedP -> User.ENEMY
                else -> {
                    if (Random.nextInt(2) == 0) User.PLAYER
                    else User.ENEMY
                }
            }
        }

        /**
         * 確率計算
         *
         * @param accuracy 確率
         * @return あたり・・・true, はずれ・・・false
         */
        fun isHit(accuracy: Int?): Boolean {
            accuracy ?: return false
            val rndInt = Random(System.currentTimeMillis()).nextInt(100)
            return rndInt < accuracy
        }
    }
}