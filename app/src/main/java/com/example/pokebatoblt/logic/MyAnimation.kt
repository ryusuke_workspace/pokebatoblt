package com.example.pokebatoblt.logic

import android.graphics.ColorMatrix
import android.graphics.ColorMatrixColorFilter
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import android.widget.ImageView
import java.util.logging.Handler

/**
 * アニメーションクラス
 */
class MyAnimation {
    companion object {
        fun fadeIn(d: Long): AlphaAnimation {
            return AlphaAnimation(0.0f, 1.0f).apply {
                duration = d
                fillAfter = true
            }
        }

        fun outPokeBall(imageView: ImageView, d: Long): ScaleAnimation {
            val colorMatrix = ColorMatrix()
            colorMatrix.setScale(1f, 0f, 0f, 1f)
            imageView.colorFilter = ColorMatrixColorFilter(colorMatrix)
            imageView.clearColorFilter()

            return ScaleAnimation(
                0.01f,
                1.0f,
                0.01f,
                1.0f,
                Animation.RELATIVE_TO_SELF,
                0.5f,
                Animation.RELATIVE_TO_SELF,
                0.8f
            ).apply {
                duration = d
                fillAfter = true
            }
        }
    }
}