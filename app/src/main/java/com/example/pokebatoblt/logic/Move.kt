package com.example.pokebatoblt.logic

import com.example.pokebatoblt.enumclass.*
import java.io.Serializable

/**
 * 技クラス
 */
class Move(
    val name: String,
    val type: Type,
    val category: MoveCategory,
    val power: Int,
    val accuracy: Int,
    var pp: Int,
    /** 優先度 */
    val priority: Int,
    /** 接触技かどうか */
    val isContact: Boolean,
    /** Triple<確率、　重複不可の状態異常、　効果を与える相手> */
    var effectAilment1: EffectAilment1? = null,
    /** Triple<確率、　重複可能の状態異常、　効果を与える相手> */
    var effectAilment2: EffectAilment2? = null,
    /** Triple<確率、　ランク(ex Aを2段階上げる 0,2,0,0,0,0)、　効果を与える相手> */
    var effectStateRank: EffectStatsRank? = null
) : Serializable {
    val maxPp = pp

    companion object {
        val SURE_HIT = 999
        val NON_POWER = 0
    }
}

class EffectAilment1(
    val accuracy: Int,
    val ailment: StatusAilment1,
    val isSelf: Boolean,
    var turnRange: IntRange? = null
)

class EffectAilment2(
    val accuracy: Int,
    val ailment: StatusAilment2,
    val isSelf: Boolean,
    var turnRange: IntRange? = null
)

class EffectStatsRank(val accuracy: Int, val rank: List<Int>, val isSelf: Boolean)