package com.example.pokebatoblt.logic

import com.example.pokebatoblt.enumclass.Type

/**
 * タイプ相性クラス
 */
class TypeCompatibility {
    companion object {
        /**
         * タイプ相性のリスト
         * 0f   効果なし
         * 1f   等倍
         * 2f   2倍
         * 3f   半減
         */
        val list = listOf(
            //         無  炎  水  雷  草  氷 　 格  毒  地  飛  超  虫 　 岩  霊  竜  悪  鋼  妖
            listOf(1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f),
            listOf(1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 3f, 0f, 1f, 1f, 3f, 1f), //無
            listOf(1f, 1f, 3f, 3f, 1f, 2f, 2f, 1f, 1f, 1f, 1f, 1f, 2f, 3f, 1f, 3f, 1f, 2f, 1f), //炎
            listOf(1f, 1f, 2f, 3f, 1f, 3f, 1f, 1f, 1f, 2f, 1f, 1f, 1f, 2f, 1f, 3f, 1f, 1f, 1f), //水
            listOf(1f, 1f, 1f, 2f, 3f, 3f, 1f, 1f, 1f, 0f, 2f, 1f, 1f, 1f, 3f, 1f, 1f, 1f, 1f), //雷
            listOf(1f, 1f, 3f, 2f, 1f, 3f, 1f, 1f, 3f, 2f, 3f, 1f, 3f, 2f, 1f, 3f, 1f, 3f, 1f), //草
            listOf(1f, 1f, 3f, 3f, 1f, 2f, 3f, 1f, 1f, 2f, 2f, 1f, 1f, 1f, 1f, 2f, 1f, 3f, 1f), //氷
            listOf(1f, 2f, 1f, 1f, 1f, 1f, 2f, 1f, 3f, 1f, 3f, 3f, 3f, 2f, 0f, 1f, 2f, 2f, 3f), //闘
            listOf(1f, 1f, 1f, 1f, 1f, 2f, 1f, 1f, 3f, 3f, 1f, 1f, 1f, 3f, 3f, 1f, 1f, 0f, 2f), //毒
            listOf(1f, 1f, 2f, 1f, 2f, 3f, 1f, 1f, 2f, 1f, 0f, 1f, 3f, 2f, 1f, 1f, 1f, 2f, 1f), //地
            listOf(1f, 1f, 1f, 1f, 3f, 2f, 1f, 2f, 1f, 1f, 1f, 1f, 2f, 3f, 1f, 1f, 1f, 3f, 1f), //飛
            listOf(1f, 1f, 1f, 1f, 1f, 1f, 1f, 2f, 2f, 1f, 1f, 3f, 1f, 1f, 1f, 1f, 0f, 3f, 1f), //超
            listOf(1f, 1f, 3f, 1f, 1f, 2f, 1f, 3f, 3f, 1f, 3f, 2f, 1f, 1f, 3f, 1f, 2f, 3f, 3f), //虫
            listOf(1f, 1f, 2f, 1f, 1f, 1f, 2f, 3f, 1f, 3f, 2f, 1f, 2f, 1f, 1f, 1f, 1f, 3f, 1f), //岩
            listOf(1f, 0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 2f, 1f, 1f, 2f, 1f, 3f, 1f, 1f), //霊
            listOf(1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 2f, 1f, 3f, 0f), //竜
            listOf(1f, 1f, 1f, 1f, 1f, 1f, 1f, 3f, 1f, 1f, 1f, 2f, 1f, 1f, 2f, 1f, 3f, 1f, 3f), //悪
            listOf(1f, 3f, 3f, 3f, 1f, 2f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 2f, 1f, 1f, 1f, 3f, 2f), //鋼
            listOf(1f, 3f, 1f, 1f, 1f, 1f, 1f, 2f, 3f, 1f, 1f, 1f, 1f, 1f, 1f, 2f, 2f, 3f, 1f)  //妖
        )
    }
}