package com.example.pokebatoblt.logic

import com.example.pokebatoblt.enumclass.*
import com.example.pokebatoblt.logic.Move.Companion.NON_POWER
import com.example.pokebatoblt.logic.Move.Companion.SURE_HIT

class MoveMap {

    fun get(vararg moveNames: String): List<Move> {
        val list = mutableListOf<Move>()
        for (name in moveNames) {
            list.add(map[name] as Move)
        }
        return list
    }

    private val map =
        mapOf(
            "" to Move(
                name = "", type = Type.NULL,
                category = MoveCategory.PHYSICAL,
                power = NON_POWER, accuracy = SURE_HIT, pp = 0,
                priority = 0, isContact = false
            ),
            "じしん" to Move(
                name = "じしん", type = Type.GROUND,
                category = MoveCategory.PHYSICAL,
                power = 100, accuracy = 100, pp = 10,
                priority = 0, isContact = false
            ),
            "ヘドロばくだん" to Move(
                name = "ヘドロばくだん", type = Type.POISON,
                category = MoveCategory.SPECIAL,
                power = 90, accuracy = 100, pp = 10,
                priority = 0, isContact = false,
                effectAilment1 = EffectAilment1(30, StatusAilment1.POISON, false)
            ),
            "こうごうせい" to Move(
                name = "こうごうせい", type = Type.GRASS,
                category = MoveCategory.STATUS,
                power = NON_POWER, accuracy = SURE_HIT, pp = 5,
                priority = 0, isContact = false,
                effectAilment2 = EffectAilment2(100, StatusAilment2.RECOVERY_HALF, true)
            ),
            "パワーウィップ" to Move(
                name = "パワーウィップ", type = Type.GRASS,
                category = MoveCategory.PHYSICAL,
                power = 120, accuracy = 85, pp = 10,
                priority = 0, isContact = true
            ),

            "かえんほうしゃ" to Move(
                name = "かえんほうしゃ", type = Type.FIRE,
                category = MoveCategory.SPECIAL,
                power = 90, accuracy = 100, pp = 15,
                priority = 0, isContact = false,
                effectAilment1 = EffectAilment1(10, StatusAilment1.BURN, false)
            ),
            "おにび" to Move(
                name = "おにび", type = Type.FIRE,
                category = MoveCategory.STATUS,
                power = NON_POWER, accuracy = 85, pp = 15,
                priority = 0, isContact = false,
                effectAilment1 = EffectAilment1(100, StatusAilment1.BURN, false)
            ),
            "きあいだま" to Move(
                name = "きあいだま", type = Type.FIGHTING,
                category = MoveCategory.SPECIAL,
                power = 120, accuracy = 70, pp = 5,
                priority = 0, isContact = false,
                effectStateRank = EffectStatsRank(10, listOf(0, 0, 0, 0, -1, 0), false)
            ),
            "りゅうのはどう" to Move(
                name = "りゅうのはどう", type = Type.DRAGON,
                category = MoveCategory.SPECIAL,
                power = 85, accuracy = 100, pp = 10,
                priority = 0, isContact = false
            ),

            "ハイドロポンプ" to Move(
                name = "ハイドロポンプ", type = Type.WATER,
                category = MoveCategory.SPECIAL,
                power = 110, accuracy = 80, pp = 5,
                priority = 0, isContact = false
            ),
            "れいとうビーム" to Move(
                name = "れいとうビーム", type = Type.ICE,
                category = MoveCategory.SPECIAL,
                power = 90, accuracy = 100, pp = 10,
                priority = 0, isContact = false,
                effectAilment1 = EffectAilment1(10, StatusAilment1.FREEZE, false)
            ),
            "がんせきふうじ" to Move(
                name = "がんせきふうじ", type = Type.ROCK,
                category = MoveCategory.PHYSICAL,
                power = 60, accuracy = 95, pp = 15,
                priority = 0, isContact = false,
                effectStateRank = EffectStatsRank(100, listOf(0, 0, 0, 0, 0, -1), false)
            ),
            "アクアジェット" to Move(
                name = "アクアジェット", type = Type.WATER,
                category = MoveCategory.PHYSICAL,
                power = 40, accuracy = 100, pp = 20,
                priority = 1, isContact = true
            )
        )
}