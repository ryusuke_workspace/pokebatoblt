package com.example.pokebatoblt.logic

import com.example.pokebatoblt.enumclass.Type

class PokemonMap {

    fun get(name: String): Pokemon {
        return map[name] as Pokemon
    }

    private val map =
        mapOf(
            "フシギバナ" to Pokemon(
                name = "フシギバナ", no = 3,
                type1 = Type.GRASS, type2 = Type.POISON,
                baseStats = listOf(80, 82, 83, 100, 100, 80),
                moveList = MoveMap().get("じしん", "ヘドロばくだん", "こうごうせい", "パワーウィップ")
            ),
            "リザードン" to Pokemon(
                name = "リザードン", no = 6,
                type1 = Type.FIRE, type2 = Type.FLYING,
                baseStats = listOf(78, 84, 78, 109, 85, 100),
                moveList = MoveMap().get("かえんほうしゃ", "おにび", "きあいだま", "りゅうのはどう")
            ),
            "カメックス" to Pokemon(
                name = "カメックス", no = 9,
                type1 = Type.WATER, type2 = Type.NULL,
                baseStats = listOf(79, 83, 100, 85, 105, 78),
                moveList = MoveMap().get("ハイドロポンプ", "れいとうビーム", "がんせきふうじ", "アクアジェット")
            ),
            "ピカチュウ" to Pokemon(
                name = "ピカチュウ", no = 25,
                type1 = Type.ELECTRIC, type2 = Type.NULL,
                baseStats = listOf(35, 55, 40, 50, 50, 90),
                moveList = MoveMap().get("", "", "", "")
            )
        )
}
