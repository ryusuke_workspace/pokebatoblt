package com.example.pokebatoblt.logic

import com.example.pokebatoblt.enumclass.User

class AtkStack(
    var firstUser: User,
    var moveP: Move?,
    var moveE: Move?
) {
    var secondUser = firstUser.reverse()

    var isCompleteFirstAtk: Boolean = false
        private set
    var isCompleteSecondAtk: Boolean = false
        private set

    fun completeAtk(user: User) = when (user) {
        firstUser -> isCompleteFirstAtk = true
        else -> isCompleteSecondAtk = true
    }

    fun changeFirstUser() {
        firstUser = firstUser.reverse()
        secondUser = secondUser.reverse()
    }

    fun firstMove() = when (firstUser) {
        User.PLAYER -> moveP
        User.ENEMY -> moveE
    }

    fun secondMove() = when (secondUser) {
        User.PLAYER -> moveP
        User.ENEMY -> moveE
    }

    fun getMove(user: User): Move? = when (user) {
        User.PLAYER -> moveP
        User.ENEMY -> moveE
    }
}
