package com.example.pokebatoblt.logic

import com.example.pokebatoblt.PokeApplication
import com.example.pokebatoblt.enumclass.*
import java.io.Serializable

/**
 * ポケモンクラス
 */
open class Pokemon(
    var name: String,
    var no: Int,
    var level: Int = 50,
    var type1: Type,
    var type2: Type = Type.NULL,
    var baseStats: List<Int>,
    var moveList: List<Move>
) : Serializable {
    val MAX_LEVEL: Int = 100
    val drawableId: Int =
        PokeApplication.instance.resources.getIdentifier(
            "n$no",
            "drawable",
            PokeApplication.instance.packageName
        )

    var gender: Gender = Gender.MALE
    var statusAilment1: StatusAilment1 = StatusAilment1.NULL
    var statusAilment2List = mutableListOf<StatusAilment2>()
    val realStats = mutableListOf(0, 0, 0, 0, 0, 0)
    var currentH: Int = 0
        set(value) {
            field = when {
                value > getRealStats(Stats.H) -> getRealStats(Stats.H)
                value < 0 -> {
                    this.statusAilment1 = StatusAilment1.FAINTING
                    0
                }
                else -> value
            }
        }

    var statsRank = mutableListOf(0, 0, 0, 0, 0, 0)

    var effortValue: List<Int> = listOf(0, 0, 0, 0, 0, 0)
        set(value) {
            field = value
            calStats()
        }
    var individualValue: List<Int> = listOf(31, 31, 31, 31, 31, 31)
        set(value) {
            field = value
            calStats()
        }

    var sleepTurn = 0

    init {
        calStats()
    }

    private fun calStats() {
        // HP計算式
        //(種族値×2+個体値+努力値÷4)×レベル÷100+レベル+10
        realStats[Stats.H.ordinal] =
            ((baseStats[Stats.H.ordinal] * 2
                    + individualValue[Stats.H.ordinal]
                    + effortValue[Stats.H.ordinal] / 4).toFloat()
                    * level.toFloat() / MAX_LEVEL.toFloat()).toInt() + level + 10

        currentH = realStats[Stats.H.ordinal]

        // HP以外のステータス計算式
        // { (種族値×2+個体値+努力値÷4)×レベル÷100+5 }×せいかく補正
        for (i in Stats.A.ordinal..Stats.S.ordinal) {
            realStats[i] = (((
                    (baseStats[i] * 2 + individualValue[i] + effortValue[i] / 4).toFloat()
                            * level.toFloat() / MAX_LEVEL.toFloat()).toInt() + 5
                    ).toFloat() * 1.0f).toInt()
        }
    }

    fun getRealStats(stats: Stats): Int {
        return realStats[stats.ordinal]
    }

    fun getRankAppliedStats(state: Stats): Int {
        val list = mutableListOf<Int>()
        for (i in Stats.H.ordinal..Stats.S.ordinal) {
            list.add((realStats[i].toFloat() * Calculator.calStatsRankMultiplier(statsRank[i])).toInt())
        }
        return list[state.ordinal]
    }
}