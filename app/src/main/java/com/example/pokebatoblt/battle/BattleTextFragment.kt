package com.example.pokebatoblt.battle

import android.content.Context
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.pokebatoblt.R
import com.example.pokebatoblt.enumclass.Effective
import com.example.pokebatoblt.enumclass.StatusAilment1
import com.example.pokebatoblt.enumclass.User
import com.example.pokebatoblt.logic.Move
import com.example.pokebatoblt.logic.MyAnimation
import com.example.pokebatoblt.logic.Pokemon
import kotlinx.android.synthetic.main.fragment_battle_text.*

/**
 * バトルのテキストを表示するフラグメント
 */
class BattleTextFragment : Fragment() {

    private var callback: BattleTextFragmentCallback? = null

    private val ANIME_DURATION: Long = 1000

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_battle_text, container, false)

    fun addPutPokemonLog(pokemon: Pokemon) = addLog("いけ！ ${pokemon.name}", duration = 250L)

    fun addAtkLog(pokemon: Pokemon, move: Move?) {
        move ?: return
        addLog("${pokemon.name}の ${move.name}")
    }

    fun addDoesNotHitLog(pokemon: Pokemon) = addLog("しかし ${pokemon.name}の 攻撃は 外れた")
    fun addCriticalLog() = addLog("きゅうしょに あたった")
    fun addEffectiveLog(effective: Effective) {
        val text: String = when (effective) {
            Effective.NORMAL -> ""
            Effective.SUPER_EFFECTIVE -> "効果は ばつぐんだ"
            Effective.NOT_VERY_EFFECTIVE -> "効果は いまひとつのようだ"
            Effective.NOT_EFFECTIVE -> "効果は ないようだ…"
        }
        addLog(text)
    }

    fun addStatsAilmentLog(pokemon: Pokemon, statusAilment1: StatusAilment1?) {
        statusAilment1 ?: return
        val text = when (statusAilment1) {
            StatusAilment1.BURN -> "やけどを おった！"
            StatusAilment1.FREEZE -> "こおりついた！"
            StatusAilment1.PARALYSIS -> "まひして 技が 出にくくなった！"
            StatusAilment1.POISON,
            StatusAilment1.BAD_POISON -> "どくに かかった！"
            StatusAilment1.SLEEP -> "眠ってしまった！"
            StatusAilment1.FAINTING -> "戦闘不能に なってしまった！"
            else -> return
        }
        addLog("${pokemon.name}は $text")
    }

    fun addBurnLog(pokemon: Pokemon) = addLog("${pokemon.name}は やけどの ダメージを受けた")
    fun addFreezeLog(pokemon: Pokemon) = addLog("${pokemon.name}は こおって動けない")
    fun addRecoverFreezeLog(pokemon: Pokemon) = addLog("${pokemon.name}の こおりが 溶けた")
    fun addPoisonLog(pokemon: Pokemon) = addLog("${pokemon.name}は どくの ダメージを受けた")
    fun addParalysisLog(pokemon: Pokemon) = addLog("${pokemon.name}は 体がしびれて 技を 出すことができない")
    fun addSleepLog(pokemon: Pokemon) = addLog("${pokemon.name}は ぐうぐう 眠っている")
    fun addRecoverSleepLog(pokemon: Pokemon) = addLog("${pokemon.name}は 目をさました")

    fun addFaintingLog(user: User, pokemon: Pokemon) {
        when (user) {
            User.PLAYER -> addLog("${user.name}の ${pokemon.name}は 倒れてしまった")
            User.ENEMY -> addLog("${user.name}の ${pokemon.name}を 倒した")
        }
    }

    /** 1行ずつログを追加する */
    private fun addLog(vararg texts: String, duration: Long = ANIME_DURATION) {
        callback?.onLoadingBattleTextFragment(true)
        for ((i, text) in texts.withIndex()) {
            if (text.isNotEmpty()) {
                Handler().postDelayed({ addLine(text, duration) }, duration * (i))
            }
        }
        Handler().postDelayed(
            { callback?.onLoadingBattleTextFragment(false) },
            (duration.toFloat() * (texts.size.toFloat() + 0.5f)).toLong()
        )
    }

    private fun addLine(text: String, duration: Long) {
        when {
            battle_text_1.text == "" -> {
                battle_text_1.text = text
                battle_text_1.startAnimation(MyAnimation.fadeIn(duration))
            }
            battle_text_2.text == "" -> {
                battle_text_2.text = text
                battle_text_2.startAnimation(MyAnimation.fadeIn(duration))
            }
            battle_text_3.text == "" -> {
                battle_text_3.text = text
                battle_text_3.startAnimation(MyAnimation.fadeIn(duration))
            }
            else -> {
                battle_text_1.text = battle_text_2.text
                battle_text_2.text = battle_text_3.text
                battle_text_3.text = text
                battle_text_3.startAnimation(MyAnimation.fadeIn(duration))
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BattleTextFragmentCallback) {
            callback = context
        } else {
            throw RuntimeException("$context must implement BattleTextFragmentCallback")
        }
    }

    override fun onDetach() {
        super.onDetach()
        callback = null
    }

    interface BattleTextFragmentCallback {
        fun onLoadingBattleTextFragment(isLoading: Boolean)
    }
}
