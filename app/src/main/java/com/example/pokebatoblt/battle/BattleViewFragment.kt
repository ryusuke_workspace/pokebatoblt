package com.example.pokebatoblt.battle

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.pokebatoblt.R
import com.example.pokebatoblt.enumclass.Stats
import com.example.pokebatoblt.enumclass.User
import com.example.pokebatoblt.logic.MyAnimation
import com.example.pokebatoblt.logic.Pokemon
import com.example.pokebatoblt.viewparts.StatsBarLayout
import kotlinx.android.synthetic.main.fragment_battle_view.*

/**
 * 戦闘画面を表示するフラグメント
 */
class BattleViewFragment : Fragment() {

    private var callback: BattleViewFragmentCallback? = null

    private var statsBarLayoutCallbackE = StatsBarLayoutCallbackP()
    private var statsBarLayoutCallbackP = StatsBarLayoutCallbackE()
    var mainPokemonP: Pokemon? = null
    var mainPokemonE: Pokemon? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_battle_view, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        callback?.onViewCreated()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BattleViewFragmentCallback) {
            callback = context
        } else {
            throw RuntimeException("$context must implement BattleViewFragmentCallback")
        }
    }

    override fun onDetach() {
        super.onDetach()
        callback = null
    }


    private fun getStatsBarLayout(user: User): StatsBarLayout {
        return when (user) {
            User.PLAYER -> player_stats_layout
            User.ENEMY -> enemy_state_layout
        }
    }

    /**
     * ポケモンを入れ替える
     *
     * @param user 入れ替えるユーザー
     * @param pokemon 入れ替え先のポケモン
     */
    fun replacePokemon(user: User, pokemon: Pokemon) {
        when (user) {
            User.PLAYER -> mainPokemonP = pokemon
            User.ENEMY -> mainPokemonE = pokemon
        }
        getStatsBarLayout(user).run {
            hpPair = Pair(pokemon.currentH, pokemon.getRealStats(Stats.H))
            name = pokemon.name
            level = pokemon.level
            ailment1 = pokemon.statusAilment1
        }
        replacePokemonImg(user, pokemon.drawableId)
    }

    private fun replacePokemonImg(user: User, id: Int) {
        callback?.onLoadingBattleViewFragment(true)
        var bmp = BitmapFactory.decodeResource(resources, id)
        when (user) {
            User.PLAYER -> {
                val matrix = Matrix()
                matrix.preScale(-1f, 1f)
                bmp = Bitmap.createBitmap(bmp, 0, 0, bmp.width, bmp.height, matrix, false)
                player_poke_img.setImageBitmap(bmp)
                player_poke_img.startAnimation(MyAnimation.outPokeBall(player_poke_img, 200))
            }
            User.ENEMY -> {
                enemy_poke_img.setImageBitmap(bmp)
                enemy_poke_img.startAnimation(MyAnimation.outPokeBall(enemy_poke_img, 200))
            }
        }
        Handler().postDelayed({ callback?.onLoadingBattleViewFragment(false) }, 1000)
    }

    /**
     * HPバーを更新する
     * HPに変動があった場合に使用
     *
     * @param user ユーザー
     * @param pokemon ポケモン
     */
    fun updateCurrentHp(user: User, pokemon: Pokemon) {
        getStatsBarLayout(user).decreaseHpBar(getStatsBarLayout(user).hpPair.first - pokemon.currentH)
    }

    /**
     * ステータスバーの状態異常を更新する
     *
     * @param user ユーザー
     * @param pokemon ポケモン
     */
    fun updateAilment(user: User, pokemon: Pokemon) {
        getStatsBarLayout(user).ailment1 = pokemon.statusAilment1
    }


    interface BattleViewFragmentCallback {
        fun onViewCreated()
        fun onLoadingBattleViewFragment(isLoading: Boolean)
    }

    fun setStatsBarLayoutCallback() {
        player_stats_layout.setCallback(statsBarLayoutCallbackP)
        enemy_state_layout.setCallback(statsBarLayoutCallbackE)
    }


    internal inner class StatsBarLayoutCallbackP : StatsBarLayout.StatsBarLayoutCallback {
        override fun onStatsBarLoading(isLoading: Boolean) {
            callback?.onLoadingBattleViewFragment(isLoading)
        }
    }

    internal inner class StatsBarLayoutCallbackE : StatsBarLayout.StatsBarLayoutCallback {
        override fun onStatsBarLoading(isLoading: Boolean) {
            callback?.onLoadingBattleViewFragment(isLoading)
        }
    }
}
