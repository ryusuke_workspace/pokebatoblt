package com.example.pokebatoblt.battle

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AlertDialog
import com.example.pokebatoblt.MainActivity
import com.example.pokebatoblt.R
import com.example.pokebatoblt.enumclass.*
import com.example.pokebatoblt.logic.*
import kotlin.random.Random

/**
 * バトル画面
 */
class BattleActivity : AppCompatActivity(), Runnable, BattleCmdFragment.BattleCmdFragmentCallback,
    BattleViewFragment.BattleViewFragmentCallback, BattleTextFragment.BattleTextFragmentCallback {

    private lateinit var battleViewFragment: BattleViewFragment
    private lateinit var battleTextFragment: BattleTextFragment
    private lateinit var battleCmdFragment: BattleCmdFragment
    private val partyP: MutableList<Pokemon>
    private val partyE: MutableList<Pokemon>
    private var mainPokemonP: Pokemon
    private var mainPokemonE: Pokemon
    private fun getMainPokemon(user: User): Pokemon {
        return when (user) {
            User.PLAYER -> mainPokemonP
            User.ENEMY -> mainPokemonE
        }
    }

    private val handler = Handler()
    private var isResume = false
    private var isLoadingBattleTextFragment = false
    private var isLoadingBattleViewFragment = false
    var isBackPressed = false

    private var phases: Phases = Phases.START_TURN

    /** Triple<先攻ユーザー, 先攻ポケモンの技, 後攻ポケモンの技> */
    private var atkStack = AtkStack(User.PLAYER, null, null)

    // TODO: デバッグ用の初期化
    init {
        val pokeMapP = PokemonMap()
        val pokeMapE = PokemonMap()
        partyP = mutableListOf(pokeMapP.get("リザードン"), pokeMapP.get("カメックス"), pokeMapP.get("フシギバナ"))
        partyE = mutableListOf(pokeMapE.get("フシギバナ"), pokeMapE.get("リザードン"), pokeMapE.get("カメックス"))
        mainPokemonP = partyP.first()
        mainPokemonE = partyE.first()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_battle)

        initFragment()
    }

    override fun onResume() {
        super.onResume()
        isResume = true
        handler.post(this)
    }

    override fun onStop() {
        super.onStop()
        isResume = false
    }

    /**
     * 各フラグメントを初期化する
     */
    private fun initFragment() {
        val transaction = supportFragmentManager.beginTransaction()
        battleCmdFragment = BattleCmdFragment.newInstance(partyP.first().moveList, partyP)
        battleTextFragment = BattleTextFragment()
        battleViewFragment = BattleViewFragment()
        transaction
            .replace(R.id.battle_cmd_layout, battleCmdFragment, null)
            .replace(R.id.battle_text_layout, battleTextFragment, null)
            .replace(R.id.battle_view_layout, battleViewFragment, null)
            .commit()
    }

    override fun run() {
        val firstStrikeUser = atkStack.firstUser
        val secondStrikeUser = firstStrikeUser.reverse()

        when (phases) {
            Phases.START_TURN -> {
                // atkStack初期化
                atkStack = AtkStack(
                    Calculator.checkFirstStrikeUser(mainPokemonP, mainPokemonE),
                    null,
                    null
                )
                battleViewFragment.setStatsBarLayoutCallback()

                // 場が開いている場合ポケモンを出す
                if (mainPokemonP.statusAilment1 == StatusAilment1.FAINTING)
                    addExecQueue { battleCmdFragment.visiblePokemonCmdButton(isForced = true) }
                else if (mainPokemonE.statusAilment1 == StatusAilment1.FAINTING)
                    treadE()
                else
                    addExecQueue { battleCmdFragment.visibleMoveCmdButton() }

                phases = Phases.WAITING_INPUT
            }

            Phases.WAITING_INPUT -> {
                atkStack.firstUser = Calculator.checkFirstStrikeUser(mainPokemonP, mainPokemonE)

                val randCmd = Random.nextInt(4)
                atkStack.moveE = mainPokemonE.moveList[randCmd]
            }

            Phases.JUDGMENT_PRIORITY -> {
                battleCmdFragment.invisible()
                if (atkStack.firstMove()?.priority ?: 5 < atkStack.secondMove()?.priority ?: 0) {
                    atkStack.changeFirstUser()
                }
                phases = Phases.ATTACK1
            }

            Phases.ATTACK1 -> {
                if (atkStack.isCompleteFirstAtk) {
                    handler.postDelayed(this, 1)
                    return
                }
                attackPhase(firstStrikeUser)
            }

            Phases.ATTACK2 -> {
                if (getMainPokemon(secondStrikeUser).statusAilment1 == StatusAilment1.FAINTING) {
                    phases = Phases.WEATHER_CHECK
                    handler.postDelayed(this, 1)
                    return
                }
                if (atkStack.isCompleteSecondAtk) {
                    handler.postDelayed(this, 1)
                    return
                }
                attackPhase(secondStrikeUser)
            }

            Phases.WEATHER_CHECK -> {
                addExecQueue { phases = Phases.BURN_POISON_CHECK }
            }

            Phases.BURN_POISON_CHECK -> {
                statsAilmentDmgCheck(firstStrikeUser)
                checkPokemonFainting(firstStrikeUser)
                statsAilmentDmgCheck(secondStrikeUser)
                checkPokemonFainting(secondStrikeUser)
                phases = Phases.END_TURN
            }


            Phases.END_TURN -> {
                addExecQueue { phases = Phases.START_TURN }
            }
        }
        handler.postDelayed(this, 1)
    }

    /**
     * 状態異常のチェック
     */
    private fun statsAilmentDmgCheck(user: User) {
        val pokemon = getMainPokemon(user)
        when (pokemon.statusAilment1) {
            StatusAilment1.BURN -> {
                setCurrentHp(user, pokemon.getRealStats(Stats.H) / 16)
                addExecQueue { battleTextFragment.addBurnLog(pokemon) }
            }
            StatusAilment1.POISON -> {
                setCurrentHp(user, pokemon.getRealStats(Stats.H) / 8)
                addExecQueue { battleTextFragment.addPoisonLog(pokemon) }
            }
            StatusAilment1.BAD_POISON -> {
                setCurrentHp(user, pokemon.getRealStats(Stats.H) / 16)
                addExecQueue { battleTextFragment.addPoisonLog(pokemon) }
            }
            else -> {
            }
        }
    }

    /**
     * 攻撃を行う
     */
    private fun attackPhase(atkUser: User) {
        val atkPokemon = getMainPokemon(atkUser)

        // こおり・ねむり・まひ状態の場合
        when (atkPokemon.statusAilment1) {
            StatusAilment1.FREEZE -> {
                if (Calculator.isHit(20)) {
                    updateStatsAilment(atkUser, StatusAilment1.NULL)
                    addExecQueue { battleTextFragment.addRecoverFreezeLog(atkPokemon) }
                } else {
                    addExecQueue { battleTextFragment.addFreezeLog(atkPokemon) }
                    gotoCompleteAttackPhases(atkUser)
                    return
                }
            }
            StatusAilment1.SLEEP -> {
                if (--atkPokemon.sleepTurn < 0) {
                    updateStatsAilment(atkUser, StatusAilment1.NULL)
                    addExecQueue { battleTextFragment.addRecoverSleepLog(atkPokemon) }
                } else {
                    addExecQueue { battleTextFragment.addSleepLog(atkPokemon) }
                    gotoCompleteAttackPhases(atkUser)
                    return
                }
            }
            StatusAilment1.PARALYSIS -> {
                if (Calculator.isHit(25)) {
                    addExecQueue { battleTextFragment.addParalysisLog(atkPokemon) }
                    gotoCompleteAttackPhases(atkUser)
                    return
                }
            }
            else -> {
            }
        }

        addExecQueue { battleTextFragment.addAtkLog(atkPokemon, atkStack.getMove(atkUser)) }
        useMove(atkUser, atkStack.getMove(atkUser))
        atkStack.completeAtk(atkUser)

        addExecQueue { gotoCompleteAttackPhases(atkUser) }
    }

    private fun gotoCompleteAttackPhases(atkUser: User) {
        phases = when (atkUser) {
            atkStack.firstUser -> Phases.ATTACK2
            else -> Phases.WEATHER_CHECK
        }
    }

    /**
     * 技を使う
     */
    private fun useMove(atkUser: User, move: Move?) {
        if (move == null) return
        val defUser = atkUser.reverse()
        val atkPokemon = getMainPokemon(atkUser)
        val defPokemon = getMainPokemon(defUser)
        move.pp--

        // 命中判定
        if (move.accuracy != Move.SURE_HIT && !Calculator.isHit(move.accuracy)) {
            addExecQueue { battleTextFragment.addDoesNotHitLog(atkPokemon) }
            return
        }

        // ダメージ計算
        if (move.category == MoveCategory.PHYSICAL || move.category == MoveCategory.SPECIAL) {
            val calDmgProtocol = Calculator.calDmg(atkPokemon, defPokemon, move)
            setCurrentHp(defUser, calDmgProtocol.dmg)

            addExecQueue { battleTextFragment.addEffectiveLog(calDmgProtocol.effective) }
            if (calDmgProtocol.isCritical)
                addExecQueue { battleTextFragment.addCriticalLog() }

            if (defPokemon.currentH <= 0) return
        }


        // 重複不可の状態異常の追加効果
        val ailment1 = move.effectAilment1?.ailment ?: StatusAilment1.NULL
        val effectedUser1 =
            if (move.effectAilment1?.isSelf == true) atkUser
            else atkUser.reverse()
        val effectedPokemon1 = getMainPokemon(effectedUser1)
        if (effectedPokemon1.statusAilment1 == StatusAilment1.NULL && Calculator.isHit(move.effectAilment1?.accuracy)) {
            updateStatsAilment(effectedUser1, ailment1)
            addExecQueue { battleTextFragment.addStatsAilmentLog(effectedPokemon1, ailment1) }
        }


        // 重複可の追加効果
        val effectedUser2 =
            if (move.effectAilment2?.isSelf == true) atkUser
            else atkUser.reverse()

        if (move.effectAilment2?.ailment == StatusAilment2.RECOVERY_HALF)
            setCurrentHp(effectedUser2, atkPokemon.realStats[Stats.H.ordinal] / 2 * -1)
    }

    /**
     * 現在のHPを更新する
     */
    private fun setCurrentHp(user: User, dmg: Int) {
        val pokemon = getMainPokemon(user)
        pokemon.currentH -= dmg
        addExecQueue { battleViewFragment.updateCurrentHp(user, pokemon) }
    }

    private fun updateStatsAilment(user: User, statusAilment: StatusAilment1) {
        val pokemon = getMainPokemon(user)
        pokemon.statusAilment1 = statusAilment
        addExecQueue { battleViewFragment.updateAilment(user, pokemon) }
    }

    /**
     * ひんしを判定する
     */
    private fun checkPokemonFainting(user: User) {
        val pokemon = getMainPokemon(user)
        if (pokemon.currentH <= 0) {
            addExecQueue { battleTextFragment.addFaintingLog(user, pokemon) }
            battleJudgment()
        }
    }

    /**
     * 戦闘終了判定
     */
    private fun battleJudgment() {
        var countP = 0
        for (poke in partyP)
            if (poke.currentH > 0)
                countP++
        var countE = 0
        for (poke in partyE)
            if (poke.currentH > 0)
                countE++

        val text =
            when {
                countP == 0 -> "LOSE"
                countE == 0 -> "WIN"
                else -> ""
            }

        if (text.isNotEmpty()) {
            isResume = false
            AlertDialog.Builder(this).run {
                setMessage(text)
                setCancelable(false)
                setPositiveButton("OK") { _, _ ->
                    startActivity(
                        Intent(this@BattleActivity, MainActivity::class.java)
                    )
                    this@BattleActivity.finish()
                }
                show()
            }
        }
    }

    private fun treadP(i: Int) {
        mainPokemonP = partyP[i]
        addExecQueue {
            battleViewFragment.replacePokemon(User.PLAYER, mainPokemonP)
            battleCmdFragment.updateButton(mainPokemonP.moveList)
            battleCmdFragment.visibleMoveCmdButton()
            battleTextFragment.addPutPokemonLog(mainPokemonP)
        }
    }

    private fun treadE() {
        for (pokemon in partyE) {
            if (pokemon.currentH > 0) {
                mainPokemonE = pokemon
                break
            }
        }
        addExecQueue {
            battleViewFragment.replacePokemon(User.ENEMY, mainPokemonE)
            battleTextFragment.addPutPokemonLog(mainPokemonE)
        }
    }


    /** BattleCmdFragmentCallback */
    override fun onSelectedMoveCmdButton(moveButtonId: Int) {
        val randCmd = Random.nextInt(4)
        atkStack.moveP = mainPokemonP.moveList[moveButtonId]
        atkStack.moveE = mainPokemonE.moveList[randCmd]

        phases = Phases.JUDGMENT_PRIORITY
    }

    /** BattleCmdFragmentCallback */
    override fun onSelectedPokemonCmdButtonForced(pokemonButtonId: Int) = treadP(pokemonButtonId)
    override fun onSelectedPokemonCmdButton(pokemonButtonId: Int) {
        treadP(pokemonButtonId)
        phases = Phases.JUDGMENT_PRIORITY
    }

    /** BattleCmdFragmentCallback */
    override fun onPressedEscapeButton() = onBackPressed()
    override fun onPressedPokemonButton() = battleCmdFragment.visiblePokemonCmdButton()
    override fun onPressedBackPokemonButton() = battleCmdFragment.visibleMoveCmdButton()

    /** BattleViewFragmentCallback */
    override fun onViewCreated() {
        addExecQueue { battleTextFragment.addPutPokemonLog(mainPokemonP) }
        addExecQueue { battleViewFragment.replacePokemon(User.PLAYER, mainPokemonP) }

        addExecQueue { battleTextFragment.addPutPokemonLog(mainPokemonE) }
        addExecQueue { battleViewFragment.replacePokemon(User.ENEMY, mainPokemonE) }
    }

    /** BattleViewFragmentCallback */
    override fun onLoadingBattleViewFragment(isLoading: Boolean) {
        isLoadingBattleViewFragment = isLoading
    }

    /** BattleTextFragmentCallback */
    override fun onLoadingBattleTextFragment(isLoading: Boolean) {
        this.isLoadingBattleTextFragment = isLoading
    }

    /**
     * 順番に処理を行うためのキュー
     */
    private var execQueue = mutableListOf<() -> Unit>()

    /**
     * 処理をキューに追加する
     */
    private fun addExecQueue(function: () -> Unit) {
        execQueue.add(function)
        var r: Runnable? = null
        r = Runnable {
            if (!isResume || isLoadingBattleTextFragment || isLoadingBattleViewFragment) {
                handler.postDelayed(r, 1)
            } else if (execQueue.isNotEmpty()) {
                execQueue[0]()
                execQueue = execQueue.drop(1).toMutableList()
                handler.postDelayed(r, 1)
            }
        }
        handler.post(r)
    }

    override fun onBackPressed() {
        if (isBackPressed) return
        isBackPressed = true
        addExecQueue {
            AlertDialog.Builder(this).run {
                setMessage("終了しますか？")
                setPositiveButton("OK") { _, _ -> finish() }
                setNegativeButton("キャンセル", null)
                setCancelable(false)
                show()
            }
            isBackPressed = false
        }
    }
}

