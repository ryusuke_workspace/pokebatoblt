package com.example.pokebatoblt.battle

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.pokebatoblt.R
import com.example.pokebatoblt.logic.Move
import com.example.pokebatoblt.logic.Pokemon
import com.example.pokebatoblt.viewparts.MoveCmdButtonLayout
import com.example.pokebatoblt.viewparts.PokemonCmdButtonLayout
import kotlinx.android.synthetic.main.fragment_battle_cmd.*
import java.io.Serializable

private const val ARG_MOVE_LIST = "move_list"
private const val ARG_POKEMON_LIST = "pokemon_list"

/**
 * バトルのコマンドを表示するフラグメント
 */
class BattleCmdFragment : Fragment() {

    private var callback: BattleCmdFragmentCallback? = null

    private lateinit var moveButtonList: List<MoveCmdButtonLayout>
    private lateinit var pokemonButtonList: List<PokemonCmdButtonLayout>
    var moveList: List<Move>? = null
    var pokemonList: List<Pokemon>? = null

    companion object {
        fun newInstance(moveList: List<Move>, pokemonList: List<Pokemon>) =
            BattleCmdFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(ARG_MOVE_LIST, moveList as Serializable)
                    putSerializable(ARG_POKEMON_LIST, pokemonList as Serializable)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        @Suppress("UNCHECKED_CAST")
        arguments?.let {
            moveList = it.getSerializable(ARG_MOVE_LIST) as List<Move>
            pokemonList = it.getSerializable(ARG_POKEMON_LIST) as List<Pokemon>
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_battle_cmd, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        moveButtonList = listOf(cmd_button_0, cmd_button_1, cmd_button_2, cmd_button_3)
        pokemonButtonList = listOf(cmd_button_4, cmd_button_5, cmd_button_6)
        updateButton(moveList)
        invisible()

        cmd_button_escape.setOnClickListener { callback?.onPressedEscapeButton() }
    }

    /**
     * ボタンの情報を更新する
     */
    fun updateButton(moveList: List<Move>?) {
        updateMoveButton(moveList)
        updatePokemonButton()
    }

    private fun updateMoveButton(moveList: List<Move>? = this.moveList) {
        this.moveList = moveList
        for ((i, button) in moveButtonList.withIndex()) {
            button.setOnClickListener { callback?.onSelectedMoveCmdButton(i) }
            button.move = moveList?.get(i)
        }
    }

    private fun updatePokemonButton(pokemonList: List<Pokemon>? = this.pokemonList) {
        this.pokemonList = pokemonList
        for ((i, button) in pokemonButtonList.withIndex()) {
            button.setOnClickListener { callback?.onSelectedPokemonCmdButton(i) }
            button.pokemon = pokemonList?.get(i)
        }
    }

    fun visibleMoveCmdButton() {
        updateMoveButton()
        for (button in moveButtonList)
            button.visibility = View.VISIBLE
        for (button in pokemonButtonList)
            button.visibility = View.INVISIBLE
        cmd_button_escape.visibility = View.VISIBLE
        cmd_button_pokemon.visibility = View.VISIBLE
        cmd_button_pokemon.text = "ポケモン"
        cmd_button_pokemon.setOnClickListener { callback?.onPressedPokemonButton() }

    }

    fun visiblePokemonCmdButton(isForced: Boolean = false) {
        updatePokemonButton()
        for (button in moveButtonList)
            button.visibility = View.INVISIBLE
        for (button in pokemonButtonList)
            button.visibility = View.VISIBLE

        if (isForced) {
            cmd_button_escape.visibility = View.INVISIBLE
            cmd_button_pokemon.visibility = View.INVISIBLE
            for ((i, button) in pokemonButtonList.withIndex())
                button.setOnClickListener {
                    callback?.onSelectedPokemonCmdButtonForced(i)
                }
        } else {
            cmd_button_escape.visibility = View.VISIBLE
            cmd_button_pokemon.visibility = View.VISIBLE
            for ((i, button) in pokemonButtonList.withIndex())
                button.setOnClickListener {
                    callback?.onSelectedPokemonCmdButton(i)
                }
            cmd_button_pokemon.text = "もどる"
            cmd_button_pokemon.setOnClickListener { callback?.onPressedBackPokemonButton() }
        }
    }

    fun invisible() {
        for (button in moveButtonList)
            button.visibility = View.INVISIBLE
        for (button in pokemonButtonList)
            button.visibility = View.INVISIBLE
        cmd_button_escape.visibility = View.INVISIBLE
        cmd_button_pokemon.visibility = View.INVISIBLE
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BattleCmdFragmentCallback) {
            callback = context
        } else {
            throw RuntimeException("$context must implement BattleCmdFragmentCallback")
        }
    }

    override fun onDetach() {
        super.onDetach()
        callback = null
    }

    /** 名前がやばい */
    interface BattleCmdFragmentCallback {
        fun onSelectedMoveCmdButton(moveButtonId: Int)
        fun onSelectedPokemonCmdButtonForced(pokemonButtonId: Int)
        fun onSelectedPokemonCmdButton(pokemonButtonId: Int)
        fun onPressedEscapeButton()
        fun onPressedPokemonButton()
        fun onPressedBackPokemonButton()
    }
}
