package com.example.pokebatoblt.viewparts

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import com.example.pokebatoblt.R
import kotlinx.android.synthetic.main.battle_view_stats_bar.view.*
import android.os.Handler
import android.os.Looper
import androidx.core.content.ContextCompat
import com.example.pokebatoblt.enumclass.StatusAilment1
import com.example.pokebatoblt.enumclass.User
import java.lang.RuntimeException

class StatsBarLayout : LinearLayout {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleInt: Int)
            : super(context, attrs, defStyleInt)

    private var callback: StatsBarLayoutCallback? = null
    private val hpBarHandler = Handler(Looper.getMainLooper())

    init {
        this.orientation = VERTICAL
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.battle_view_stats_bar, null)
        this.addView(view)
    }

    /** Pair<現在HP, 最大HP> */
    var hpPair: Pair<Int, Int> = Pair(-1, -1)
        set(value) {
            field = value
            val currentHp = value.first
            val maxHp = value.second
            updateHpBar(currentHp, maxHp)
        }

    var name: String? = null
        set(value) {
            field = value
            stats_bar_name.text = value
        }

    var level: Int = 0
        set(value) {
            field = value
            stats_bar_level.text = "Lv.$value"
        }

    var ailment1: StatusAilment1 = StatusAilment1.NULL
        set(value) {
            field = value
            stats_bar_ailment.text = ailment1.text
            stats_bar_ailment.setTextColor(ContextCompat.getColor(context, value.colorId))
        }

    private fun updateHpBar(currentHp: Int, maxHp: Int) {
        stats_bar_hp_text.text = "$currentHp / $maxHp"
        val progressRate: Float = currentHp.toFloat() / maxHp.toFloat() * 1000f
        stats_ber_hp_bar_green.progress = progressRate.toInt()
        stats_ber_hp_bar_yellow.progress = progressRate.toInt()
        stats_ber_hp_bar_red.progress = progressRate.toInt()
        when {
            progressRate <= 100f -> {
                stats_ber_hp_bar_green.visibility = View.GONE
                stats_ber_hp_bar_yellow.visibility = View.GONE
                stats_ber_hp_bar_red.visibility = View.VISIBLE
            }
            progressRate <= 500f -> {
                stats_ber_hp_bar_green.visibility = View.GONE
                stats_ber_hp_bar_yellow.visibility = View.VISIBLE
                stats_ber_hp_bar_red.visibility = View.GONE
            }
            else -> {
                stats_ber_hp_bar_green.visibility = View.VISIBLE
                stats_ber_hp_bar_yellow.visibility = View.GONE
                stats_ber_hp_bar_red.visibility = View.GONE
            }
        }
    }

    fun decreaseHpBar(dmg: Int) {
        callback?.onStatsBarLoading(true)

        var count = dmg
        val decrement =
            if (dmg >= 0) 1
            else -1
        var r: Runnable? = null
        r = Runnable {
            if (decrement == 1 && count > 0 && hpPair.first > 0) {
                hpPair = Pair(hpPair.first - decrement, hpPair.second)
                updateHpBar(hpPair.first, hpPair.second)
                count -= decrement
                hpBarHandler.postDelayed(r, 20)
            } else if (decrement == -1 && count < 0 && hpPair.first < hpPair.second) {
                hpPair = Pair(hpPair.first - decrement, hpPair.second)
                updateHpBar(hpPair.first, hpPair.second)
                count -= decrement
                hpBarHandler.postDelayed(r, 20)
            } else if (hpPair.first == 0) {
                callback?.onStatsBarLoading(false)
            } else {
                callback?.onStatsBarLoading(false)
            }
        }
        hpBarHandler.post(r)
    }


    fun setCallback(callback: StatsBarLayoutCallback) {
        this.callback = callback
    }

    interface StatsBarLayoutCallback {
        fun onStatsBarLoading(isLoading: Boolean)
    }
}