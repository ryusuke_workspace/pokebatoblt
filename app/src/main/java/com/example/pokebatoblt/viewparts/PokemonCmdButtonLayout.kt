package com.example.pokebatoblt.viewparts

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import com.example.pokebatoblt.R
import com.example.pokebatoblt.enumclass.Stats
import com.example.pokebatoblt.logic.Pokemon
import kotlinx.android.synthetic.main.battle_pokemon_cmd_button.view.*

private const val DISABLE_ALPHA = 0.5f
private const val ENABLE_ALPHA = 1.0f

class PokemonCmdButtonLayout : LinearLayout {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleInt: Int)
            : super(context, attrs, defStyleInt)

    init {
        this.orientation = VERTICAL
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.battle_pokemon_cmd_button, null)
        this.addView(view)
    }

    var pokemon: Pokemon? = null
        set(value) {
            field = value ?: return
            pokemon_cmd_button_img.background = ContextCompat.getDrawable(context, value.drawableId)
            pokemon_cmd_button_name.text = value.name
            pokemon_cmd_button_type1.background =
                ContextCompat.getDrawable(context, value.type1.iconId)
            pokemon_cmd_button_type2.background =
                ContextCompat.getDrawable(context, value.type2.iconId)
            pokemon_cmd_button_hp.text = "${value.currentH}/${value.getRealStats(Stats.H)}"
            pokemon_cmd_button_stats_ailment1.text = value.statusAilment1.text
            pokemon_cmd_button_stats_ailment1.setTextColor(
                ContextCompat.getColor(context, value.statusAilment1.colorId)
            )

            if (value.currentH <= 0) {
                this.isEnabled = false
                this.alpha = DISABLE_ALPHA
            } else {
                this.isEnabled = true
                this.alpha = ENABLE_ALPHA
            }
        }
}