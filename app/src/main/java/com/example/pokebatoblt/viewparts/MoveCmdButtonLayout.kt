package com.example.pokebatoblt.viewparts

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import com.example.pokebatoblt.PokeApplication
import com.example.pokebatoblt.R
import com.example.pokebatoblt.logic.Move
import kotlinx.android.synthetic.main.battle_move_cmd_button.view.*


private const val DISABLE_ALPHA = 0.5f
private const val ENABLE_ALPHA = 1.0f

class MoveCmdButtonLayout : LinearLayout {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleInt: Int)
            : super(context, attrs, defStyleInt)

    init {
        this.orientation = VERTICAL
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.battle_move_cmd_button, null)
        this.addView(view)
    }

    var move: Move? = null
        set(value) {
            field = value ?: return

            cmd_button_name.text = value.name

            cmd_button_type_img.background =
                PokeApplication.instance.resources.getDrawable(value.type.iconId, null)

            cmd_button_move_category.text = value.category.text

            val power =
                if (move?.power == Move.NON_POWER) "-"
                else move?.power.toString()
            cmd_button_power.text = "威力 $power"

            val accuracy =
                if (move?.accuracy == Move.SURE_HIT) "-"
                else move?.accuracy.toString()
            cmd_button_accuracy.text = "命中 $accuracy"

            cmd_button_pp.text = "pp %d / %d".format(move?.pp, move?.maxPp)

            if (value.pp <= 0) {
                this.isEnabled = false
                this.alpha = DISABLE_ALPHA
            } else {
                this.isEnabled = true
                this.alpha = ENABLE_ALPHA
            }
        }
}