package com.example.pokebatoblt.enumclass

/**
 * ポケモンのステータス
 */
enum class Stats {
    H, A, B, C, D, S
}