package com.example.pokebatoblt.enumclass

/**
 * タイプ相性による効果
 */
enum class Effective(string: String) {
    NORMAL(""),
    SUPER_EFFECTIVE("効果抜群"),
    NOT_VERY_EFFECTIVE("効果いまひとつ"),
    NOT_EFFECTIVE("効果なし")
}