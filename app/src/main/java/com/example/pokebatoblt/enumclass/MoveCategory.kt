package com.example.pokebatoblt.enumclass

/**
 * 技のカテゴリ
 */
enum class MoveCategory(val text: String) {
    PHYSICAL("物理"),
    SPECIAL("特殊"),
    STATUS("変化")
}