package com.example.pokebatoblt.enumclass

import com.example.pokebatoblt.R

/**
 * 重複不可能な状態異常
 */
enum class StatusAilment1(val text: String, val colorId: Int) {
    NULL("", R.color.colorPrimaryDark),
    BURN("やけど", R.color.burn),
    FREEZE("こおり", R.color.freeze),
    PARALYSIS("まひ", R.color.paralysis),
    POISON("どく", R.color.poison),
    BAD_POISON("もうどく", R.color.poison),
    SLEEP("ねむり", R.color.sleep),
    FAINTING("ひんし", R.color.fainting);
}

/**
 * 重複可能な状態異常・効果
 */
enum class StatusAilment2(val text: String) {
    CONFUSION("こんらん"),
    RECOVERY_HALF("1/2回復");
}