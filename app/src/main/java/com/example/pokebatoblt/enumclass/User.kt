package com.example.pokebatoblt.enumclass

/**
 * ユーザー
 */
enum class User {
    PLAYER,
    ENEMY;

    fun reverse(): User {
        return when (this) {
            PLAYER -> ENEMY
            ENEMY -> PLAYER
        }
    }
}