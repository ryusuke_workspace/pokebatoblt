package com.example.pokebatoblt.enumclass

/**
 * 性別
 */
enum class Gender {
    MALE,
    FEMALE,
    UNKNOWN
}