package com.example.pokebatoblt.enumclass

import com.example.pokebatoblt.R

/**
 * タイプ
 */
enum class Type(val text: String, val iconId: Int) {
    NULL("NULL", R.drawable.type_normal),
    NORMAL("ノーマル", R.drawable.type_normal),
    FIRE("ほのお", R.drawable.type_fire),
    WATER("みず", R.drawable.type_water),
    ELECTRIC("でんき", R.drawable.type_electric),
    GRASS("くさ", R.drawable.type_grass),
    ICE("こおり", R.drawable.type_ice),
    FIGHTING("かくとう", R.drawable.type_fighting),
    POISON("どく", R.drawable.type_poison),
    GROUND("じめん", R.drawable.type_ground),
    FLYING("ひこう", R.drawable.type_flying),
    PSYCHIC("エスパー", R.drawable.type_psychic),
    BUG("むし", R.drawable.type_bug),
    ROCK("いわ", R.drawable.type_rock),
    GHOST("ゴースト", R.drawable.type_ghost),
    DRAGON("ドラゴン", R.drawable.type_dragon),
    DARK("あく", R.drawable.type_dark),
    STEEL("はがね", R.drawable.type_steel),
    FAIRY("フェアリー", R.drawable.type_fairy)
}