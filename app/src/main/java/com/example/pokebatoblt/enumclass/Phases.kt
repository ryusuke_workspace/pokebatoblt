package com.example.pokebatoblt.enumclass

/**
 * バトル用のフェーズ
 */
enum class Phases {
    START_TURN,
    WAITING_INPUT,
    JUDGMENT_PRIORITY,
    ATTACK1,
    ATTACK2,
    WEATHER_CHECK,
    BURN_POISON_CHECK,
    END_TURN
}