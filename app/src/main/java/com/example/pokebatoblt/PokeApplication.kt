package com.example.pokebatoblt

import android.app.Application

class PokeApplication : Application() {

    companion object {
        lateinit var instance: PokeApplication
            private set
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }
}