package com.example.pokebatoblt

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import com.example.pokebatoblt.battle.BattleActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.random.Random

class MainActivity : AppCompatActivity() {

    private val miniIconList =
        listOf(
            R.drawable.n1_mini,
            R.drawable.n4_mini,
            R.drawable.n7_mini,
            R.drawable.n25_mini,
            R.drawable.n778_mini
        )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        battle_start_button.setOnClickListener {
            it.visibility = View.INVISIBLE
            val intent = Intent(this, BattleActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onResume() {
        super.onResume()

        battle_start_button.visibility = View.VISIBLE

        val miniIcon = miniIconList[Random(System.currentTimeMillis()).nextInt(miniIconList.size)]
        main_img.background = ContextCompat.getDrawable(this, miniIcon)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}
